class MergeModel extends Model {
  Model modelA, modelB;
  Range range;
  float lerpY = 0;
  float sigmoidPluck = 1;
  float sigmoidBow = 0;
  float visualY = 0;
  

  MergeModel(Model _modelA, Model _modelB, Range _range) {
    super();
    modelA = _modelA;
    modelB = _modelB;
    range = _range;
  }

  void set_data(float[] _angles, float[] _pos) {
    //lerpY = _pos[1]/0.153; // expects 0 to 1
    lerpY = map(_pos[1], range.minY, range.maxY, 0, 1);
    sigmoidPluck = 1 - 1/(1+exp(-30 * (lerpY-0.6)));
    sigmoidBow = 1/(1+exp(-30 * (lerpY-0.4)));
    visualY = _pos[1]*20  - 1;
    
    modelA.set_data(_angles, _pos);
    modelB.set_data(_angles, _pos);
    
    forceEE.x = modelA.get_force().x * sigmoidPluck + modelB.get_force().x * sigmoidBow;
    forceEE.y = modelB.get_force().y * sigmoidBow;
    
    float[] messageA = modelA.get_message();
    float[] messageB = modelB.get_message();
    message = new float[]{messageA[0]*(sigmoidPluck) + messageB[0]*sigmoidBow};
    if (sigmoidPluck < 0.99) {
      message2 = new float[]{1};
    } else {
      message2 = modelA.get_message2();
    }
  }

  void draw_graphics() {
    circle(posEE.x*width + width/2, posEE.y*height, 10);
    fill(255, 255*(1-visualY));
    rect(0, 0, width, height);
    modelA.draw_graphics();
    
    fill(255, 255*visualY);
    rect(0, 0, width, height);
    modelB.draw_graphics();
    
  }
}
