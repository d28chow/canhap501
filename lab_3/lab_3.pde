/**
 **********************************************************************************************************************
 * @file       sketch_2_Hello_Wall.pde
 * @author     Steve Ding, Colin Gallacher
 * @version    V3.0.0
 * @date       08-January-2021
 * @brief      Wall haptic example with programmed physics for a haptic wall 
 **********************************************************************************************************************
 * @attention
 *
 *
 **********************************************************************************************************************
 */

/* library imports *****************************************************************************************************/
import processing.serial.*;
import static java.util.concurrent.TimeUnit.*;
import java.util.concurrent.*;
/* end library imports *************************************************************************************************/


/* scheduler definition ************************************************************************************************/
private final ScheduledExecutorService scheduler      = Executors.newScheduledThreadPool(1);
/* end scheduler definition ********************************************************************************************/



/* device block definitions ********************************************************************************************/
Board             haplyBoard;
Device            widgetOne;
Mechanisms        pantograph;

byte              widgetOneID                         = 5;
int               CW                                  = 0;
int               CCW                                 = 1;
boolean           renderingForce                     = false;
/* end device block definition *****************************************************************************************/



/* framerate definition ************************************************************************************************/
long              baseFrameRate                       = 120;
/* end framerate definition ********************************************************************************************/



/* elements definition *************************************************************************************************/

/* Screen and world setup parameters */
float             pixelsPerMeter                      = 4000.0;
float             radsPerDegree                       = 0.01745;

/* pantagraph link parameters in meters */
float             l                                   = 0.07;
float             L                                   = 0.09;


/* end effector radius in meters */
float             rEE                                  = 0.003;//= 0.006;

/* virtual wall parameter  */
float             kWall                               = 450;
PVector           fWall                               = new PVector(0, 0);
PVector           penWall                             = new PVector(0, 0);
PVector           posWall                             = new PVector(0, 0.07);



/* generic data for a 2DOF device */
/* joint space */
PVector           angles                              = new PVector(0, 0);
PVector           torques                             = new PVector(0, 0);

/* task space */
PVector           posEE                               = new PVector(0, 0);
PVector           fEE                                 = new PVector(0, 0); 

/* device graphical position */
PVector           deviceOrigin                        = new PVector(0, 0);

/* World boundaries reference */
final int         worldPixelWidth                     = 1000;
final int         worldPixelHeight                    = 650;

import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;


/* setup section *******************************************************************************************************/
void setup() {
  haplyBoard          = new Board(this, Serial.list()[4], 0);
  widgetOne           = new Device(widgetOneID, haplyBoard);
  pantograph          = new Pantograph();


  widgetOne.set_mechanism(pantograph);

  widgetOne.add_actuator(1, CCW, 2);
  widgetOne.add_actuator(2, CW, 1);

  widgetOne.add_encoder(1, CCW, 241, 10752, 2);
  widgetOne.add_encoder(2, CW, -61, 10752, 1);

  widgetOne.device_set_parameters();

  size(400, 400);
  frameRate(baseFrameRate);

  oscP5 = new OscP5(this, 8003);
  myRemoteLocation = new NetAddress("127.0.0.1", 8002);

  SimulationThread st = new SimulationThread();
  scheduler.scheduleAtFixedRate(st, 1, 1, MILLISECONDS);
}

void draw() {
}

void oscEvent(OscMessage msg) {
  /* print the address pattern and the typetag of the received OscMessage */
  
  Object args[] = msg.arguments();
  fWall.set(0, 0);
  if (args.length == 2) {
    fWall.set((float)args[0], (float)args[1]);
  }
}

class SimulationThread implements Runnable {

  public void run() {
    /* put haptic simulation code here, runs repeatedly at 1kHz as defined in setup */

    renderingForce = true;

    if (haplyBoard.data_available()) {
      OscMessage myMessage = new OscMessage("/pos");

      /* send the message */


      widgetOne.device_read_data();

      float[] device_angles = widgetOne.get_device_angles();
      angles.set(device_angles); 

      float[] device_pos = widgetOne.get_device_position(angles.array());
      posEE.set(device_pos);

      //println(device_pos);
      myMessage.add(device_pos);
      posEE.set(device_to_graphics(posEE)); 
      
      ///* haptic wall force calculation */
      //fWall.set(0, 0);

      //for (Ring ring : rings) {
      //  ring.update();
      //}


      ////create_walls();

      fEE = (fWall.copy()).mult(-1);
      fEE.set(graphics_to_device(fEE));
      ///* end haptic wall force calculation */
      oscP5.send(myMessage, myRemoteLocation);
    }
    //double x = -2.0*((double)mouseX/width - 0.5)*2.0;
    //double y = -2.0*((double)mouseY/height - 0.5)*2.0;
    //fEE = new PVector((int)x, (int)y);

    torques.set(widgetOne.set_device_torques(fEE.array()));
    widgetOne.device_write_torques();


    renderingForce = false;
  }
}
/* end simulation section **********************************************************************************************/

void create_walls() {
  penWall.set(0, (posWall.y - (posEE.y + rEE)));
  println(posWall.y - (posEE.y + rEE));
  if (penWall.y < 0) {
    fWall = fWall.add(penWall.mult(-kWall));
  }
}


PVector device_to_graphics(PVector deviceFrame) {
  return deviceFrame.set(-deviceFrame.x, deviceFrame.y);
}

PVector graphics_to_device(PVector graphicsFrame) {
  return graphicsFrame.set(-graphicsFrame.x, graphicsFrame.y);
}
