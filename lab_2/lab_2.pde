/**
 **********************************************************************************************************************
 * @file       sketch_2_Hello_Wall.pde
 * @author     Steve Ding, Colin Gallacher
 * @version    V3.0.0
 * @date       08-January-2021
 * @brief      Wall haptic example with programmed physics for a haptic wall 
 **********************************************************************************************************************
 * @attention
 *
 *
 **********************************************************************************************************************
 */

/* library imports *****************************************************************************************************/
import processing.serial.*;
import static java.util.concurrent.TimeUnit.*;
import java.util.concurrent.*;
/* end library imports *************************************************************************************************/


/* scheduler definition ************************************************************************************************/
private final ScheduledExecutorService scheduler      = Executors.newScheduledThreadPool(1);
/* end scheduler definition ********************************************************************************************/



/* device block definitions ********************************************************************************************/
Board             haplyBoard;
Device            widgetOne;
Mechanisms        pantograph;

byte              widgetOneID                         = 5;
int               CW                                  = 0;
int               CCW                                 = 1;
boolean           renderingForce                     = false;
/* end device block definition *****************************************************************************************/



/* framerate definition ************************************************************************************************/
long              baseFrameRate                       = 120;
/* end framerate definition ********************************************************************************************/



/* elements definition *************************************************************************************************/

/* Screen and world setup parameters */
float             pixelsPerMeter                      = 4000.0;
float             radsPerDegree                       = 0.01745;

/* pantagraph link parameters in meters */
float             l                                   = 0.07;
float             L                                   = 0.09;


/* end effector radius in meters */
float             rEE                                  = 0.003;//= 0.006;

/* virtual wall parameter  */
float             kWall                               = 450;
PVector           fWall                               = new PVector(0, 0);
PVector           penWall                             = new PVector(0, 0);
PVector           posWall                             = new PVector(0, 0.07);



/* generic data for a 2DOF device */
/* joint space */
PVector           angles                              = new PVector(0, 0);
PVector           torques                             = new PVector(0, 0);

/* task space */
PVector           posEE                               = new PVector(0, 0);
PVector           fEE                                 = new PVector(0, 0); 

/* device graphical position */
PVector           deviceOrigin                        = new PVector(0, 0);

/* World boundaries reference */
final int         worldPixelWidth                     = 1000;
final int         worldPixelHeight                    = 650;


/* graphical elements */
PShape pGraph, joint, endEffector;
PShape wall;
/* end elements definition *********************************************************************************************/

class Ring {
  float r;
  float holeAngle;
  float holeSize = 0.01;

  Ring(float _r, float _holeAngle) {
    r = _r;
    holeAngle = _holeAngle;
  }

  void update() {
    float d = dist(posEE.x, posEE.y, 0, 0);
    float theta = atan(posEE.x/posEE.y);
    float thetaD = abs(degrees(theta) - holeAngle);
    if (d > r && d < r + holeSize) {
      if (thetaD < 5) return;
      d = (d-r)*3;
      penWall.set(d*sin(theta), d*cos(theta));
      fWall = fWall.add(penWall.mult(kWall));
    }
  }
  
  void draw() {
    stroke(1);
    circle(deviceOrigin.x, deviceOrigin.y, (r + rEE*2)*pixelsPerMeter*2);
    noStroke();
    circle(deviceOrigin.x + (r+holeSize)*pixelsPerMeter*sin(radians(holeAngle)), (r+holeSize)*pixelsPerMeter*cos(radians(holeAngle)), 50);
  }
}


ArrayList<Ring> rings;

/* setup section *******************************************************************************************************/
void setup() {
  /* put setup code here, run once: */

  /* screen size definition */
  size(1000, 650);

  /* device setup */

  /**  
   * The board declaration needs to be changed depending on which USB serial port the Haply board is connected.
   * In the base example, a connection is setup to the first detected serial device, this parameter can be changed
   * to explicitly state the serial port will look like the following for different OS:
   *
   *      windows:      haplyBoard = new Board(this, "COM10", 0);
   *      linux:        haplyBoard = new Board(this, "/dev/ttyUSB0", 0);
   *      mac:          haplyBoard = new Board(this, "/dev/cu.usbmodem1411", 0);
   */
  haplyBoard          = new Board(this, Serial.list()[3], 0);
  widgetOne           = new Device(widgetOneID, haplyBoard);
  pantograph          = new Pantograph();

  rings = new ArrayList<Ring>();
  for (int i=0; i<8; i++) {
    Ring r = new Ring((1 - i/8.0) * 0.11 + 0.02, (random(1)-0.5)*70);
    rings.add(r);
  }
  
  widgetOne.set_mechanism(pantograph);

  widgetOne.add_actuator(1, CCW, 2);
  widgetOne.add_actuator(2, CW, 1);

  widgetOne.add_encoder(1, CCW, 241, 10752, 2);
  widgetOne.add_encoder(2, CW, -61, 10752, 1);

  widgetOne.device_set_parameters();


  /* visual elements setup */
  background(0);
  deviceOrigin.add(worldPixelWidth/2, 0);

  /* create pantagraph graphics */
  create_pantagraph();
  



  /* setup framerate speed */
  frameRate(baseFrameRate);


  /* setup simulation thread to run at 1kHz */
  SimulationThread st = new SimulationThread();
  scheduler.scheduleAtFixedRate(st, 1, 1, MILLISECONDS);
}
/* end setup section ***************************************************************************************************/

/* draw section ********************************************************************************************************/
void draw() {
  /* put graphical code here, runs repeatedly at defined framerate in setup, else default at 60fps: */
  if (renderingForce == false) {
    background(255); 
    update_animation(angles.x*radsPerDegree, angles.y*radsPerDegree, posEE.x, posEE.y);
  }
}
/* end draw section ****************************************************************************************************/

/* simulation section **************************************************************************************************/
class SimulationThread implements Runnable {

  public void run() {
    /* put haptic simulation code here, runs repeatedly at 1kHz as defined in setup */

    renderingForce = true;

    if (haplyBoard.data_available()) {
      /* GET END-EFFECTOR STATE (TASK SPACE) */
      widgetOne.device_read_data();

      angles.set(widgetOne.get_device_angles()); 
      posEE.set(widgetOne.get_device_position(angles.array()));
      posEE.set(device_to_graphics(posEE)); 


      /* haptic wall force calculation */
      fWall.set(0, 0);
      
      for (Ring ring : rings) {
        ring.update();
      }


      //create_walls();

      fEE = (fWall.copy()).mult(-1);
      fEE.set(graphics_to_device(fEE));
      /* end haptic wall force calculation */
    }
    //double x = -2.0*((double)mouseX/width - 0.5)*2.0;
    //double y = -2.0*((double)mouseY/height - 0.5)*2.0;
    //fEE = new PVector((int)x, (int)y);

    torques.set(widgetOne.set_device_torques(fEE.array()));
    widgetOne.device_write_torques();


    renderingForce = false;
  }
}
/* end simulation section **********************************************************************************************/

void create_walls() {
  penWall.set(0, (posWall.y - (posEE.y + rEE)));
  println(posWall.y - (posEE.y + rEE));
  if (penWall.y < 0) {
    fWall = fWall.add(penWall.mult(-kWall));
  }
}

/* helper functions section, place helper functions here ***************************************************************/
void create_pantagraph() {
  float lAni = pixelsPerMeter * l;
  float LAni = pixelsPerMeter * L;
  float rEEAni = pixelsPerMeter * rEE;

  pGraph = createShape();
  pGraph.beginShape();
  pGraph.fill(0);
  pGraph.stroke(1);
  pGraph.strokeWeight(10);

  pGraph.vertex(deviceOrigin.x, deviceOrigin.y);
  pGraph.vertex(deviceOrigin.x, deviceOrigin.y);
  pGraph.vertex(deviceOrigin.x, deviceOrigin.y);
  pGraph.vertex(deviceOrigin.x, deviceOrigin.y);
  pGraph.endShape(CLOSE);

  joint = createShape(ELLIPSE, deviceOrigin.x, deviceOrigin.y, rEEAni, rEEAni);
  joint.setStroke(color(0));

  endEffector = createShape(ELLIPSE, deviceOrigin.x, deviceOrigin.y, 2*rEEAni, 2*rEEAni);
  endEffector.setStroke(color(0));
  strokeWeight(5);
}


PShape create_wall() {


  return createShape(ELLIPSE, deviceOrigin.x, deviceOrigin.y, (0.1 + rEE*2)*pixelsPerMeter*2, (0.1 + rEE*2)*pixelsPerMeter*2);
}


void update_animation(float th1, float th2, float xE, float yE) {
  background(255);

  float lAni = pixelsPerMeter * l;
  float LAni = pixelsPerMeter * L;

  xE = pixelsPerMeter * xE;
  yE = pixelsPerMeter * yE;

  th1 = 3.14 - th1;
  th2 = 3.14 - th2;

  pGraph.setVertex(1, deviceOrigin.x + lAni*cos(th1), deviceOrigin.y + lAni*sin(th1));
  pGraph.setVertex(3, deviceOrigin.x + lAni*cos(th2), deviceOrigin.y + lAni*sin(th2));
  pGraph.setVertex(2, deviceOrigin.x + xE, deviceOrigin.y + yE);

  //shape(pGraph);
  //shape(joint);
  for (Ring ring : rings) {
    ring.draw();
  }
  


  translate(xE, yE);
  shape(endEffector);
}


PVector device_to_graphics(PVector deviceFrame) {
  return deviceFrame.set(-deviceFrame.x, deviceFrame.y);
}


PVector graphics_to_device(PVector graphicsFrame) {
  return graphicsFrame.set(-graphicsFrame.x, graphicsFrame.y);
}



/* end helper functions section ****************************************************************************************/
